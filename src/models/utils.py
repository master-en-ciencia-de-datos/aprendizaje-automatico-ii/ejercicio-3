import numpy as np
from scipy.cluster.hierarchy import dendrogram
from sklearn.metrics import accuracy_score, f1_score, \
    silhouette_score
from tqdm import tqdm
import pandas as pd
import os

from src.data.generate_data import twospirals

N_POINTS = 400
NOISE = 0.7
NUM_CV = 10


def plot_raw(ax):
    X, y = twospirals(n_points=N_POINTS, noise=NOISE)
    from src.visualization.plot_prediction import plot_prediction
    ax = plot_prediction(X, y, ax)
    return ax


def calc_acc_f1_silhouette(K=None, iterations=1, model=None):
    _list_acc_score = []
    _list_micro_f1_score = []
    _list_macro_f1_score = []
    _list_weighted_f1_score = []
    _list_silhouette = []
    for iteration in tqdm(range(iterations), leave=False, desc=str(K)):
        X, y = twospirals(n_points=N_POINTS, noise=NOISE)
        labels = model.fit_predict(X)

        _list_acc_score.append(accuracy_score(y, labels))
        _list_micro_f1_score.append(f1_score(y, labels,
                                             average='micro'))
        _list_macro_f1_score.append(f1_score(y, labels,
                                             average='macro'))
        _list_weighted_f1_score.append(f1_score(y, labels,
                                                average='weighted'))
        # calculating silhouette
        test_silhouette = silhouette_score(X, labels)
        _list_silhouette.append(test_silhouette)
        # _list_silhouette.append(np.mean(test_silhouette))

    acc_score = np.mean(_list_acc_score)
    micro_f1_score = np.mean(_list_micro_f1_score)
    macro_f1_score = np.mean(_list_macro_f1_score)
    weighted_f1_score = np.mean(_list_weighted_f1_score)
    silhouette = np.mean(_list_silhouette)
    _f1_score = [micro_f1_score, macro_f1_score, weighted_f1_score]

    return acc_score, _f1_score, silhouette


def get_model_labels(model):
    X, y = twospirals(n_points=N_POINTS, noise=NOISE)
    labels = model.fit_predict(X)
    return X, labels


def write_score(list_K, list_acc_score, list_micro_f1_score,
                list_macro_f1_score, list_weighted_f1_score,
                list_silhouette_score, file_prefix=None):

    data = {'K': list_K,
            'Accuracy': list_acc_score,
            'F1 Score Micro': list_micro_f1_score,
            'F1 Score Macro': list_macro_f1_score,
            'F1 Score Weighted': list_weighted_f1_score,
            'Silhouette': list_silhouette_score,
            }

    df = pd.DataFrame(data, columns=['K', 'Accuracy', 'F1 Score Micro',
                                     'F1 Score Macro', 'F1 Score Weighted',
                                     'Silhouette'])
    # print(df)

    df.to_csv(os.path.join(os.path.abspath(os.path.dirname(__file__)),
                           "data", file_prefix + "_scores.csv"))


def plot_dendrogram(model, **kwargs):
    # Create linkage matrix and then plot the dendrogram

    # create the counts of samples under each node
    counts = np.zeros(model.children_.shape[0])
    n_samples = len(model.labels_)
    for i, merge in enumerate(model.children_):
        current_count = 0
        for child_idx in merge:
            if child_idx < n_samples:
                current_count += 1  # leaf node
            else:
                current_count += counts[child_idx - n_samples]
        counts[i] = current_count

    linkage_matrix = np.column_stack([model.children_, model.distances_,
                                      counts]).astype(float)

    # Plot the corresponding dendrogram
    dendrogram(linkage_matrix, **kwargs)


def fit_and_plot_dendogram(model, ax):
    X, y = twospirals(n_points=N_POINTS, noise=NOISE)
    # plot the top three levels of the dendrogram
    model = model(distance_threshold=0, n_clusters=None)
    model.fit(X)
    plot_dendrogram(model, truncate_mode='level', p=3, ax=ax)
