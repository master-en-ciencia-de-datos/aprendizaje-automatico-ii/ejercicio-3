"""
The idea is to analyze how does k-means, hierarchical clustering and spectral
 clustering behave in the two spirals problem.

    1. Calculate accuracy and F1 for K between 2 and 5. Export to a CSV.
    2. Calculate Silhouette and export the result to a .csv in order to select
    the best value.
"""
import os

import matplotlib.pyplot as plt
from sklearn.cluster import KMeans, AgglomerativeClustering, SpectralClustering

from src.models.utils import calc_acc_f1_silhouette, \
    get_model_labels, write_score, fit_and_plot_dendogram, plot_raw
from src.visualization.plot_prediction import plot_prediction
from src.visualization.utils import cm2inch

models = {
    "k_means":
        {"name": "K-medias", "model": KMeans, "list_K": list(range(2, 6)),
         "n_rows": 1, "n_cols": 4},
    "hierarchical_clustering":
        {"name": "Clústering Jerárquico Aglomerativo",
         "model": AgglomerativeClustering, "list_K": list(range(2, 6)),
         "n_rows": 1, "n_cols": 4},
    "spectral_clustering":
        {"name": "Clústering Espectral",
         "model": SpectralClustering, "list_K": list(range(2, 6)),
         "n_rows": 1, "n_cols": 4},
}

fig_raw, ax_raw = plt.subplots(1, 1, figsize=(cm2inch(7.5, 8)))
ax_raw = plot_raw(ax=ax_raw)
fig_raw.suptitle("Generación inicial")
fig_raw.tight_layout(rect=[0, 0.03, 1, 0.95])
fig_raw.savefig(os.path.join(os.path.abspath(os.path.dirname(__file__)),
                             "images", "raw_data"))

for model_key in models:
    print("Running {}".format(model_key))
    list_acc_score, list_micro_f1_score, list_macro_f1_score = [], [], []
    list_weighted_f1_score, list_silhouette_score = [], []
    _model = models[model_key]["model"]
    list_K = models[model_key]["list_K"]
    n_rows, n_cols = models[model_key]["n_rows"], models[model_key]["n_cols"]
    model_name = models[model_key]["name"]
    fig, (axs) = plt.subplots(n_rows, n_cols,
                              figsize=(cm2inch(7.5 * n_cols, 8 * n_rows)))

    for (ax, K) in zip(fig.axes, list_K):
        if _model == SpectralClustering:
            model = _model(n_clusters=K, assign_labels="discretize",
                           affinity='nearest_neighbors', random_state=0)
        elif _model == AgglomerativeClustering:
            model = _model(n_clusters=K, linkage='single')
        else:
            model = _model(K)
        acc, f1_score, silhouette = calc_acc_f1_silhouette(K, iterations=1,
                                                           model=model)
        list_acc_score.append(acc)
        list_micro_f1_score.append(f1_score[0])
        list_macro_f1_score.append(f1_score[1])
        list_weighted_f1_score.append(f1_score[2])
        list_silhouette_score.append(silhouette)

        # Plotting
        X, labels = get_model_labels(model=model)
        plot_prediction(X, labels, ax)
        ax.set_title("K = {}".format(K))

    if _model == AgglomerativeClustering:
        fig2, ax2 = plt.subplots()
        fig2.suptitle('Dendrograma para Clústering Jerárquico Aglomerativo')
        fit_and_plot_dendogram(_model, ax2)
        ax2.set_xlabel("Número de puntos por nodo")
        fig2.savefig(os.path.join(os.path.abspath(
            os.path.dirname(__file__)), "images",
            "dendograms", "none_clusters"))

    write_score(list_K, list_acc_score, list_micro_f1_score,
                list_macro_f1_score, list_weighted_f1_score,
                list_silhouette_score, file_prefix=model_key)

    fig.suptitle("Agrupamiento con {}".format(model_name))
    fig.tight_layout(rect=[0, 0.03, 1, 0.95])
    fig.subplots_adjust(top=0.85)
    fig.savefig(os.path.join(os.path.abspath(os.path.dirname(__file__)),
                             "images", model_key + "_predictions"))
plt.show()
