from sklearn.cluster import SpectralClustering
from sklearn.metrics import make_scorer, accuracy_score, f1_score, \
    silhouette_score

from src.data.generate_data import twospirals

N_POINTS = 400
NOISE = 0.7
NUM_CV = 10

X, y = twospirals(n_points=N_POINTS, noise=NOISE)
labels = SpectralClustering.fit_predict(X)
