import matplotlib.pyplot as plt


def plot_raw_spirals(X, y):
    plt.title('training set')
    plt.plot(X[y == 0, 0], X[y == 0, 1], '.', label='class 1')
    plt.plot(X[y == 1, 0], X[y == 1, 1], '.', label='class 2')
    plt.legend()
    plt.show()
