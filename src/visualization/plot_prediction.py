import matplotlib.pyplot as plt
import numpy as np


def plot_prediction(X, labels, ax=None):
    if ax is None:
        fig, ax = plt.subplots(1, 1)
    for k in np.unique(labels):
        indexes = [i for i, x in enumerate(labels) if x == k]
        _X = [x for i, x in enumerate(X) if i in indexes]
        x, y = np.array(_X)[:, 0], np.array(_X)[:, 1]
        ax.scatter(x, y, s=5)

    return ax
